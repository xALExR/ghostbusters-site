-- --------------------------------------------------------
-- Сервер:                       127.0.0.1
-- Версія сервера:               5.5.45 - MySQL Community Server (GPL)
-- ОС сервера:                   Win32
-- HeidiSQL Версія:              9.3.0.5058
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for kursach
CREATE DATABASE IF NOT EXISTS `kursach` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kursach`;

-- Dumping structure for таблиця kursach.agents
CREATE TABLE IF NOT EXISTS `agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` char(50) DEFAULT NULL,
  `surname` char(50) DEFAULT NULL,
  `specialization` char(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Дані для експорту не вибрані
-- Dumping structure for процедура kursach.get_new_order
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_new_order`()
BEGIN
SELECT o.address, t.name, g.ghost_type, g.dangerous, o.id 
	FROM `towns` AS t JOIN `orders`AS o JOIN `ghosts` AS g
		ON g.id=o.ghost_id AND t.id=o.town_id WHERE o.agent_id IS NULL;
END//
DELIMITER ;

-- Dumping structure for процедура kursach.get_order
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_order`(IN `ag_id` INT)
BEGIN
SELECT o.address, t.name, g.ghost_type, g.dangerous, o.id FROM `towns` AS t JOIN `orders`AS o JOIN `ghosts` AS g JOIN `agents` AS a 
	ON o.agent_id=a.id AND g.id=o.ghost_id AND t.id=o.town_id WHERE a.id=ag_id;
END//
DELIMITER ;

-- Dumping structure for таблиця kursach.ghosts
CREATE TABLE IF NOT EXISTS `ghosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ghost_type` char(15) NOT NULL DEFAULT '0',
  `dangerous` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дані для експорту не вибрані
-- Dumping structure for таблиця kursach.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` char(30) NOT NULL DEFAULT '0',
  `town_id` int(11) NOT NULL DEFAULT '0',
  `agent_id` int(11) DEFAULT NULL,
  `ghost_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_orders_towns` (`town_id`),
  KEY `FK_orders_agents` (`agent_id`),
  KEY `FK_orders_ghosts` (`ghost_id`),
  CONSTRAINT `FK_orders_agents` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`),
  CONSTRAINT `FK_orders_ghosts` FOREIGN KEY (`ghost_id`) REFERENCES `ghosts` (`id`),
  CONSTRAINT `FK_orders_towns` FOREIGN KEY (`town_id`) REFERENCES `towns` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- Дані для експорту не вибрані
-- Dumping structure for таблиця kursach.towns
CREATE TABLE IF NOT EXISTS `towns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дані для експорту не вибрані
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
