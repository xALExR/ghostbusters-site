#!/home/PycharmProjects/env/bin/python3.5
from app import create_app

app = create_app()
print("Project available on address: 127.0.0.1:5000/course_project")

if __name__ == "__main__":
    app.run(debug=True)

