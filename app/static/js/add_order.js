/**
 * Created by xALEx on 29.04.2016.
 */
function add_order(order_id) {
    $.ajax({
        type: 'POST',	   // тип запроса
        dataType: 'json', // тип загружаемых данных
        url: '/add_order',    // URL по которому должен обрабатываться запрос
        data: JSON.stringify({id: order_id}),
        success: function (data) {
            $("#answer" + order_id.toString()).text(data['answer'])
        },
        error: function (data) {
            $("#answer" + order_id.toString()).text(data['answer'])
        }
    });

}