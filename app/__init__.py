from flask import Flask
from flask_login import LoginManager
from app.database import db
current_db = db
my_login_manager = LoginManager()


def create_app():
    app = Flask('kursach')
    app.config.from_object('app.setting')
    from app.core.views import my_blueprint
    app.register_blueprint(my_blueprint)
    current_db.init_app(app)
    with app.test_request_context():
        current_db.create_all()
    my_login_manager.init_app(app)
    my_login_manager.login_view = "login"
    my_login_manager.session_protection = "strong"
    # admin user = login: admin, password = admin1
    return app
