from flask import render_template, redirect, flash, url_for, request, g, jsonify, Blueprint
from flask_login import login_user, login_required, logout_user, current_user, current_app

from app import my_login_manager as login_manager
from .forms import LoginForm, RegistrationForm
from .models import Agent, Order
from ..setting import STATIC_FOLDER_PATH, TEMPLATE_FOLDER_PATH


my_blueprint = Blueprint(
    'kursach', __name__,
    url_prefix='/course_project',
    template_folder=TEMPLATE_FOLDER_PATH,
    static_folder=STATIC_FOLDER_PATH,
    static_url_path='/static'
    )


@my_blueprint.before_request
def all_order():
    current_app.jinja_env.globals['all_orders'] = Order.all_order()
    current_app.jinja_env.globals['all_free_orders'] = Order.all_free_order()


@my_blueprint.route('/', methods=['GET', 'POST'])
@my_blueprint.route('/index/', methods=['GET', 'POST'])
def index():
    return render_template('kursach/index.html', methods=['GET'])


@login_manager.user_loader
def load_user(user_id):
    return Agent.query.get(int(user_id))


@my_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        flash("You are already logged in")
        return redirect(url_for('index'))
    login_form = LoginForm()
    if request.method == "POST":
        if login_form.validate_on_submit():
            user = Agent.get_user(login_form.login.data, login_form.password.data)
            if user is not None:
                login_user(user, login_form.remember_me.data)
                flash('Login success')
                return redirect(request.args.get('next') or url_for('kursach.index'))
            else:
                flash('Current user not exist')
    return render_template('kursach/login.html', title='Sing in', form=login_form)


@my_blueprint.route('/registration', methods=['GET', 'POST'])
def registration():
    if current_user.is_authenticated:
        flash("You are already logged in")
        return redirect(url_for('kursach.index'))
    reg_form = RegistrationForm()
    if request.method == "POST":
        if reg_form.validate_on_submit():
            registration_user = Agent.add_user(
                reg_form.login.data, reg_form.password.data, reg_form.username.data,
                reg_form.surname.data, reg_form.specialization.data, reg_form.email.data, reg_form.rank.data
            )
            if registration_user is True:
                user = Agent.get_user(reg_form.login.data, reg_form.password.data)
                login_user(user)
                return redirect(request.args.get('next') or url_for('index'))
            else:
                flash("User already exist or incorrect data")
    return render_template('kursach/registration.html', title='Registration', form=reg_form)


@my_blueprint.before_request
def before_request():
    g.user = current_user


@my_blueprint.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('kursach.index'))


@my_blueprint.route('/my_orders', methods=['GET'])
@login_required
def my_orders():
    orders = Agent.db_call_proc('get_order', [current_user.id])
    return render_template('kursach/my_orders.html', title='My orders', orders_list=orders)


@my_blueprint.route('/new_orders', methods=['GET', 'POST'])
@login_required
def new_orders():
    new_order = Agent.db_call_proc('get_new_order')
    return render_template('kursach/new_orders.html', title='New order', orders=new_order)


@my_blueprint.route('/add_order', methods=['POST'])
def add_order():
    add_request = request.get_json(force=True)
    order_id = add_request['id']
    query = Agent.confirm_oder(order_id, g.user.id)
    if query is True:
        return jsonify({'answer': 'Order successfully added'})
    else:
        return jsonify({'answer': 'Error'})


@my_blueprint.route('/add_new_order', methods=['GET', 'POST'])
def add_new_order():
    order_form = Order.select_all_select_field()
    if order_form.validate_on_submit():
        new_order = Order.insert_order(order_form.address.data, order_form.town.data[0], order_form.ghost.data[0])
        if new_order:
            flash("Order successful added")
            return redirect(request.args.get('next') or url_for('kursach.index'))
        else:
            flash("Error please try again")
            return redirect(url_for("kursach.add_new_order"))
    return render_template('kursach/add_new_order.html', form=order_form)
