from wtforms import BooleanField, PasswordField, StringField, IntegerField, SelectField
from wtforms.fields.html5 import EmailField
from wtforms import validators
from flask_wtf import FlaskForm


class LoginForm(FlaskForm):
    login = StringField('Username')
    password = PasswordField('Password', [
        validators.Length(min=6, max=24), validators.DataRequired()
    ])
    remember_me = BooleanField('remember_me', default=False)


class RegistrationForm(FlaskForm):
    login = StringField('Login', [validators.Length(min=4, max=32)])
    password = PasswordField('Password', [
            validators.Length(min=6, max=32), validators.DataRequired(),
            validators.EqualTo('confirm', message='Passwords must match')
        ])
    confirm = PasswordField('Repeat Password')
    username = StringField('Username', [validators.Length(min=4, max=50)])
    surname = StringField('Surname', [validators.Length(min=4, max=50)])
    specialization = StringField('Specialization', [validators.Length(min=4, max=20)])
    email = EmailField('Email address', [validators.DataRequired(), validators.Email()])
    rank = IntegerField('Agent rank', [validators.NumberRange(min=1, max=5)])


class AddNewOrder(FlaskForm):
    town = SelectField("Town", coerce=str)
    address = StringField("Address", [validators.Length(min=5)])
    ghost = SelectField("Ghost", coerce=str)
