from sqlalchemy import func

from app import current_db as db
from .forms import AddNewOrder
from .views import g


class Agent(db.Model):
    __tablename__ = "agents"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.VARCHAR(32))
    password = db.Column(db.VARCHAR(255))
    name = db.Column(db.CHAR(50))
    surname = db.Column(db.CHAR(50))
    specialization = db.Column(db.CHAR(20))
    email = db.Column(db.VARCHAR(255))
    rank = db.Column(db.Integer)
    oder = db.relationship("Order", uselist=False, backref='agent')

    def __init__(self, login, password, name, surname, specialization, email, rank):
        self.login = login
        self.password = password
        self.name = name
        self.surname = surname
        self.specialization = specialization
        self.email = email
        self.rank = rank

    def __repr__(self):
        return str('User: id=%d, login=%s, password=%s, name=%s, surname=%s, specialization=%s, rank=%d' % (self.id,
                   self.login, self.password, self.name, self.surname, self.specialization, self.rank))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    @classmethod
    def get_user(cls, login, password):
        hash_password = pass_hash(password, login)
        user = Agent.query.filter_by(login=login, password=hash_password).first()
        return user

    @classmethod
    def add_user(cls, login, password, name, surname, specialization, mail, rank):
        user = Agent.get_user(login, password)
        if user is not None:
            user = False
        else:
            new_user = Agent(login, pass_hash(password, login), name, surname, specialization, mail, rank)
            db.session.add(new_user)
            db.session.commit()
            user = True
        return user

    @staticmethod
    def get_my_odrers():
        user_id = session['user_id']
        query = db.session.query(Agent, Order)
        query = query.filter_by(Agent.id == user_id and Order.agent_id == user_id)
        records = query.all()
        return records

    @staticmethod
    def get_new_orders():
        query = Order.query.filter_by(agent_id='NULL').all()
        return query

    @staticmethod
    def db_call_proc(name, agr=()):
        connection = db.engine.raw_connection()
        cursor = connection.cursor()
        cursor.callproc(name, agr)
        results = cursor.fetchall()
        cursor.close()
        connection.commit()
        orders = []
        for node in results:
            dic = dict()
            dic['address'] = node[0]
            dic['town'] = node[1]
            dic['ghost_type'] = node[2]
            dic['dangerous'] = node[3]
            dic['id'] = node[4]
            orders.append(dic)
        return orders

    @classmethod
    def confirm_oder(cls, order_id, user_id):
        if g.user is not None:
            insert = db.session.query(Order).filter_by(id=int(order_id)).update({Order.agent_id: user_id})
            db.session.commit()
            if insert is not None:
                return True
            return False


class Order(db.Model):
    __tablename__ = "orders"
    id = db.Column(db.Integer, primary_key=True)
    town_id = db.Column(db.Integer, db.ForeignKey('towns.id'))
    address = db.Column(db.CHAR(30))
    agent_id = db.Column(db.Integer, db.ForeignKey('agents.id'))
    ghost_id = db.Column(db.Integer, db.ForeignKey('ghosts.id'))

    def __init__(self, address, town=None, ghost=None, agent=None):
        self.town_id = town
        self.address = address
        self.agent_id = agent
        self.ghost_id = ghost

    @classmethod
    def new_order(cls, town, address, agent, ghost):
        new_order = Order(town, address, agent, ghost)
        db.session.add(new_order)
        db.session.commit()

    @classmethod
    def insert_order(cls, address, town_id, ghost_type_id):
        town = db.session.query(Town).filter_by(id=town_id).first()
        ghost_type = db.session.query(Ghost).filter_by(id=ghost_type_id).first()
        order = Order(address, town=town.id, ghost=ghost_type.id)
        db.session.add(order)
        db.session.commit()
        if order is not None:
            return True
        else:
            return False

    @staticmethod
    def select_all_select_field():
        form = AddNewOrder()
        form.town.choices = [(str(t).split(",")) for t in db.session.query(Town).order_by('name')]
        form.ghost.choices = [(str(g).split(",")) for g in db.session.query(Ghost).order_by('ghost_type')]
        return form

    @staticmethod
    def all_order():
        result, = db.session.query(func.count(Order.id)).first()
        return result

    @staticmethod
    def all_free_order():
        result, = db.session.query(func.count(Order.id)).filter_by(agent_id=None).first()
        return result


class Ghost(db.Model):
    __tablename__ = "ghosts"
    id = db.Column(db.Integer, primary_key=True)
    ghost_type = db.Column(db.CHAR(15))
    dangerous = db.Column(db.Integer)
    order = db.relationship("Order", uselist=False, backref='ghost')

    def __init__(self, ghost_type, dangerous):
        self.ghost_type = ghost_type
        self.dangerous = dangerous

    def __repr__(self):
        return "%d, Ghost type=%s Dangerous=%s" % (self.id, self.ghost_type, self.dangerous)


class Town(db.Model):
    __tablename__ = "towns"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.CHAR(20))
    orders = db.relationship("Order", uselist=False, backref='town')

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '%d, %s' % (self.id, self.name)


def pass_hash(password, login):
    import hashlib
    password = bytes("{password}{login}".format(password=password, login=login).encode('utf-8'))
    h = hashlib.md5(password)
    hash_password = h.hexdigest()
    return hash_password
