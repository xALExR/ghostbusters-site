from flask_basicauth import *


class BasedOnBasicAuth(BasicAuth):

    def __init__(self):
        super().__init__()

    def check_credentials(self, username, password):
        from model.models import Agent, pass_hash
        user = Agent.get_user(username, password)
        password = pass_hash(login=username, password=password)
        return username == user.login and password == user.password

    # def authenticate(self):
    #     from app import request
    #     auth = request.authorization
    #     return (
    #         auth and auth.type == 'basic' and
    #         self.check_credentials(auth.username, password)
    #     )
