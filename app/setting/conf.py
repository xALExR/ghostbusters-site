import datetime
import os
TESTING = False
TEMPLATE_DEBUG = True

CSRF_ENABLED = True
SECRET_KEY = '1D2132@)_21as=+uytresDFSF$34rwefsdwrefqw3223'
# ADMIN_PASS = 'd94354ac9cf3024f57409bd74eec6b4c'

PERMANENT_SESSION_LIFETIME = datetime.timedelta(days=1)
# BASIC_AUTH_USERNAME = 'admin'
# BASIC_AUTH_PASSWORD = 'admin'#'d94354ac9cf3024f57409bd74eec6b4c'
STATIC_FOLDER_PATH = os.path.join(os.path.dirname(os.path.abspath(__name__))) + '/app/static/'
TEMPLATE_FOLDER_PATH = os.path.join(os.path.dirname(os.path.abspath(__name__))) + '/app/templates/'
# BASIC_AUTH_REALM = 'coursework'
# REMEMBER_COOKIE_DURATION = datetime.timedelta(minutes=1)
# REMEMBER_COOKIE_WITHOUT_REMEMBER_ME = datetime.timedelta(seconds=0)
