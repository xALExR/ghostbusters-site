#  configuration for mysql
DB_TYPE = "mysql"
DB_NAME = "kursach"
USER_NAME = "root"
USER_PASS = "root"
HOST_NAME = "127.0.0.1"
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@127.0.0.1/kursach'
SQLALCHEMY_TRACK_MODIFICATIONS = False
